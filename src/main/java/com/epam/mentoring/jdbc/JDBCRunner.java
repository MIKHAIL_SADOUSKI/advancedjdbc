package com.epam.mentoring.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.log4j.Logger;

public class JDBCRunner {

	private static final String CREATE_LIKES_TABLE_SQL = "CREATE TABLE IF NOT EXISTS LIKES(POST_ID BIGINT(10) NOT NULL, USER_ID BIGINT(10) NOT NULL, TIMESTAMP TIMESTAMP(6) NOT NULL)";
	private static final String CREATE_POSTS_TABLE_SQL = "CREATE TABLE IF NOT EXISTS POSTS(ID BIGINT(10) NOT NULL AUTO_INCREMENT, USER_ID BIGINT(10) NOT NULL, TEXT VARCHAR(200) NOT NULL, TIMESTAMP TIMESTAMP(6) NOT NULL, PRIMARY KEY (ID))";
	private static final String CREATE_FRIENDSHIPS_TABLE_SQL = "CREATE TABLE IF NOT EXISTS FRIENDSHIPS(USER_ID_1 BIGINT(10) NOT NULL, USER_ID_2 BIGINT(10) NOT NULL, TIMESTAMP TIMESTAMP(6) NOT NULL)";
	private static final String CREATE_USERS_TABLE_SQL = "CREATE TABLE IF NOT EXISTS USERS(ID BIGINT(10) NOT NULL AUTO_INCREMENT, NAME VARCHAR(20) NOT NULL, SURNAME VARCHAR(20) NOT NULL, BIRTHDATE DATE NOT NULL, PRIMARY KEY (ID))";
	private static final String INSERT_USERS_SQL = "INSERT INTO USERS(NAME, SURNAME, BIRTHDATE) VALUES (";
	private static final String INSERT_FRIENDSHIPS_SQL = "INSERT INTO FRIENDSHIPS(USER_ID_1, USER_ID_2, TIMESTAMP) VALUES (";
	private static final String INSERT_POSTS_SQL = "INSERT INTO POSTS(USER_ID, TEXT, TIMESTAMP) VALUES (";
	private static final String INSERT_LIKES_SQL = "INSERT INTO LIKES(POST_ID, USER_ID, TIMESTAMP) VALUES (";

	private static final Logger logger = Logger.getLogger(JDBCRunner.class);
	private static final String DB_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DB_CONNECTION = "jdbc:mysql://localhost/jdbc";
	private static final String DB_USER = "root";
	private static final String DB_PASSWORD = "****";
	private static final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	public static void main(String[] args) {
		try {
			createUsersTable();
			createFriendshipsTable();
			createPostsTable();
			createLikesTable();
			insertRandomUsers(1000);
			insertRandomFriendships(1000);
			insertRandomPosts(1000);
			insertRandomLikes(1000);
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
	}

	private static void createUsersTable() throws SQLException {

		Connection dbConnection = null;
		Statement statement = null;

		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();

			statement.execute(CREATE_USERS_TABLE_SQL);

			logger.info("Table USERS is created!");
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (dbConnection != null) {
				dbConnection.close();
			}
		}
	}

	private static void createFriendshipsTable() throws SQLException {

		Connection dbConnection = null;
		Statement statement = null;

		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();

			statement.execute(CREATE_FRIENDSHIPS_TABLE_SQL);

			logger.info("Table FRIENDSHIPS is created!");
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (dbConnection != null) {
				dbConnection.close();
			}
		}
	}

	private static void createPostsTable() throws SQLException {

		Connection dbConnection = null;
		Statement statement = null;

		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();

			statement.execute(CREATE_POSTS_TABLE_SQL);

			logger.info("Table POSTS is created!");
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (dbConnection != null) {
				dbConnection.close();
			}
		}
	}

	private static void createLikesTable() throws SQLException {

		Connection dbConnection = null;
		Statement statement = null;

		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();

			statement.execute(CREATE_LIKES_TABLE_SQL);

			logger.info("Table Likes is created!");
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (dbConnection != null) {
				dbConnection.close();
			}
		}
	}

	private static void insertRandomUsers(int count) throws SQLException {

		Connection dbConnection = null;
		Statement statement = null;

		try {
			StringBuilder builder = new StringBuilder();
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			dbConnection.setAutoCommit(false);

			for (int i = 0; i < count; i++) {
				builder.append(INSERT_USERS_SQL);
				builder.append("'NAME").append(i).append("', ");
				builder.append("'SURNAME").append(i).append("', ");
				builder.append("'").append(getBirthDate()).append("'");
				builder.append(")");
				statement.addBatch(builder.toString());
				builder.setLength(0);
			}

			statement.executeBatch();
			dbConnection.commit();

			logger.info("Records inserted into USERS table!");
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (dbConnection != null) {
				dbConnection.close();
			}
		}
	}

	private static void insertRandomFriendships(int count) throws SQLException {

		Connection dbConnection = null;
		Statement statement = null;

		try {
			StringBuilder builder = new StringBuilder();
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			dbConnection.setAutoCommit(false);

			for (int i = 0; i < count; i++) {
				builder.append(INSERT_FRIENDSHIPS_SQL);
				builder.append("'").append(ThreadLocalRandom.current().nextInt(0, 1000)).append("', ");
				builder.append("'").append(ThreadLocalRandom.current().nextInt(0, 1000)).append("', ");
				builder.append("'").append(getCurrentTimeStamp()).append("'");
				builder.append(")");
				statement.addBatch(builder.toString());
				logger.debug("insert friendship statement " + builder.toString());
				builder.setLength(0);
			}

			statement.executeBatch();
			dbConnection.commit();

			logger.info("Records inserted into FRIENDSHIPS table!");
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (dbConnection != null) {
				dbConnection.close();
			}
		}
	}

	private static void insertRandomPosts(int count) throws SQLException {

		Connection dbConnection = null;
		Statement statement = null;

		try {
			StringBuilder builder = new StringBuilder();
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			dbConnection.setAutoCommit(false);

			for (int i = 0; i < count; i++) {
				builder.append(INSERT_POSTS_SQL);
				builder.append("'").append(i).append("', ");
				builder.append("'TEXT").append(i).append("', ");
				builder.append("'").append(getCurrentTimeStamp()).append("'");
				builder.append(")");
				statement.addBatch(builder.toString());
				builder.setLength(0);
			}

			statement.executeBatch();
			dbConnection.commit();

			logger.info("Records inserted into POSTS table!");
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (dbConnection != null) {
				dbConnection.close();
			}
		}
	}

	private static void insertRandomLikes(int count) throws SQLException {

		Connection dbConnection = null;
		Statement statement = null;

		try {
			StringBuilder builder = new StringBuilder();
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			dbConnection.setAutoCommit(false);

			for (int i = 0; i < count; i++) {
				builder.append(INSERT_LIKES_SQL);
				builder.append("'").append(ThreadLocalRandom.current().nextInt(0, 1000)).append("', ");
				builder.append("'").append(ThreadLocalRandom.current().nextInt(0, 1000)).append("', ");
				builder.append("'").append(getRandomTimeStamp()).append("'");
				builder.append(")");
				statement.addBatch(builder.toString());
				builder.setLength(0);
			}

			statement.executeBatch();
			dbConnection.commit();

			logger.info("Records inserted into LIKES table!");
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (dbConnection != null) {
				dbConnection.close();
			}
		}
	}

	private static Connection getDBConnection() {

		Connection dbConnection = null;

		try {
			Class.forName(DB_DRIVER);
		} catch (ClassNotFoundException e) {
			logger.error(e.getMessage(), e);
		}
		try {
			dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
			return dbConnection;
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}

		return dbConnection;
	}

	private static String getCurrentTimeStamp() {
		java.util.Date today = new java.util.Date();
		return dateFormat.format(today.getTime());
	}

	private static Timestamp getRandomTimeStamp() {
		long offset = Timestamp.valueOf("2015-01-01 00:00:00").getTime();
		long end = Timestamp.valueOf("2015-05-05 00:00:00").getTime();
		long diff = end - offset + 1;
		Timestamp rand = new Timestamp(offset + (long) (Math.random() * diff));
		return rand;
	}

	private static java.sql.Date getBirthDate() {
		java.util.Date birthDate = new java.util.Date("10/10/1977");
		java.sql.Date sqlDate = new java.sql.Date(birthDate.getTime());
		return sqlDate;
	}
}
